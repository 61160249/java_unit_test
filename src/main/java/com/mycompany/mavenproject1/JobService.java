
package com.mycompany.mavenproject1;

import java.time.LocalDate;

public class JobService {
    public static boolean checkEnableTime(LocalDate startTime, LocalDate endTime, LocalDate today){
        if(today.isBefore(startTime)){
            return false;
        }
        if(today.isAfter(endTime)){
            return false;
        }
        return true;
    }
}
